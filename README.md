# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contians all files relating to my PROC304 Final Year Computing Project known as Transcendence. 
It is a game built in the Unity Engine and is the amalgamation of all I have learnt in my degree.

Version 0.1

### How do I get set up? ###

Currently you are unable to play the game as of V0.1 as there are no files present to build.

### Who do I talk to? ###

Please contact haskinsgamesdev@gmail.com for any queries or visit https://twitter.com/HaskinsGames or https://haskinsgames.wordpress.com/ for more information.