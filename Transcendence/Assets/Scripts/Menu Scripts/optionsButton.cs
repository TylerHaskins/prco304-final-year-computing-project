﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class optionsButton : MonoBehaviour {

    public AudioSource menuButtonClick;

    void OnMenuOptionsClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
    }

    void OnGameplayOptionsClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
    }
}
