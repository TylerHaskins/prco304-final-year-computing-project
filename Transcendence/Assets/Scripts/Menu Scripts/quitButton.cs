﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class quitButton : MonoBehaviour {

    public string quitLevelName = "";
    public AudioSource menuButtonClick;

    public void OnMenuQuitClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
        Application.Quit(); //Terminates application so quit to desktop
    }

    public void OnGameplayQuitClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
        SceneManager.LoadScene(quitLevelName); //Returns player to the main menu
    }

}
