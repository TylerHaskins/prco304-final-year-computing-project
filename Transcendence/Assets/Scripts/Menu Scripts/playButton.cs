﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playButton : MonoBehaviour {

    public string levelName = "";
    public GameObject pauseMenu;
    public AudioSource menuButtonClick;

	public void OnMenuPlayClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
        SceneManager.LoadScene(levelName); //Use this function for the main menu play button to load into the tutorial level
    }

    public void OnGameplayResumeClick()
    {
        menuButtonClick.Play();
        Debug.Log("Click Sound Played"); //Play the audio clip linked with a button click
        pauseMenu.SetActive(false);
        Time.timeScale = 1.0f; //Pause menu play click during gameplay
    }
}
