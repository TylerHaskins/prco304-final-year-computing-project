﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GameUI : MonoBehaviour {

    public GameObject pauseCanvas;
    public GameObject deathCanvas;
    public GameObject endingCanvas1;
    public GameObject endingCanvas2;
    public GameObject endingCanvas3;
    public GameObject levelAdvanceCanvas;
    public GameObject levelResetCanvas;
    public GameObject pistolPlayer;
    public GameObject rocksExplosiveOne;
    public GameObject rocksExplosiveTwo;
    public Light pistolSpotlight;

    public UnityEvent HTilePuzzleHit;

    public Rigidbody rockOne;
    public Rigidbody rockTwo;
    public Rigidbody rockThree;
    public Rigidbody rockFour;
    public Rigidbody rockFive;
    public Rigidbody rockSix;

    private int health;
    private int enemyHealth;
    private int enemyHealth1;
    private int enemyHealth2;
    private int enemyHealth3;
    private int enemyHealthBoss;
    private int enemyHealth2D;
    private int enemyHealth2D2;
    private int enemyHealth2D3;
    private int enemyHealthBoss2D;
    private int blockHealth;
    private int playerScore;
    private string healthAndScoreHUD = "";
    public bool isVisible;

    public Slider playerHealthBar;
    public Slider enemyHealthBar;
    public Slider enemyHealthBar1;
    public Slider enemyHealthBar2;
    public Slider enemyHealthBar3;
    public Slider enemyHealthBarBoss;
    public Slider enemyHealthBar2D;
    public Slider enemyHealthBar2D2;
    public Slider enemyHealthBar2D3;
    public Slider enemyHealthBarBoss2D;

    public float playerHealthValue;
    public float enemyHealthValue;
    public float enemyHealthValue1;
    public float enemyHealthValue2;
    public float enemyHealthValue3;
    public float enemyHealthValueBoss;
    public float enemyHealthValue2D;
    public float enemyHealthValue2D2;
    public float enemyHealthValue2D3;
    public float enemyHealthValueBoss2D;

    void OnEnable()
    {
        PlayerBehaviour.OnUpdatePlayerHealth += HandleOnUpdatePlayerHealth;
        EnemyBehaviour.OnUpdateEnemyHealth += HandleOnUpdateEnemyHealth;
        EnemyBehaviour.OnUpdateEnemyHealth1 += HandleOnUpdateEnemyHealth1;
        EnemyBehaviour.OnUpdateEnemyHealth2 += HandleOnUpdateEnemyHealth2;
        EnemyBehaviour.OnUpdateEnemyHealth3 += HandleOnUpdateEnemyHealth3;
        EnemyBehaviour.OnUpdateEnemyHealthBoss += HandleOnUpdateEnemyHealthBoss;
        EnemyBehaviour2D.OnUpdateEnemyHealth2D += HandleOnUpdateEnemyHealth2D;
        EnemyBehav2D.OnUpdateEnemyHealth2D2 += HandleOnUpdateEnemyHealth2D2;
        EnemyBehaviour2DSecond.OnUpdateEnemyHealth2D3 += HandleOnUpdateEnemyHealth2D3;
        EnemyBehaviourBoss2D.OnUpdateEnemyHealthBoss2D += HandleOnUpdateEnemyHealthBoss2D;
        BreakableBlockFunctionality.OnUpdateBlockHealth += HandleOnUpdateBlockHealth;
        ItemCollision.OnCollide += HandleOnCollide;
    }

    void OnDisable()
    {
        PlayerBehaviour.OnUpdatePlayerHealth -= HandleOnUpdatePlayerHealth;
        EnemyBehaviour.OnUpdateEnemyHealth -= HandleOnUpdateEnemyHealth;
        EnemyBehaviour.OnUpdateEnemyHealth1 -= HandleOnUpdateEnemyHealth1;
        EnemyBehaviour.OnUpdateEnemyHealth2 -= HandleOnUpdateEnemyHealth2;
        EnemyBehaviour.OnUpdateEnemyHealth3 -= HandleOnUpdateEnemyHealth3;
        EnemyBehaviour.OnUpdateEnemyHealthBoss -= HandleOnUpdateEnemyHealthBoss;
        EnemyBehaviour2D.OnUpdateEnemyHealth2D -= HandleOnUpdateEnemyHealth2D;
        EnemyBehav2D.OnUpdateEnemyHealth2D2 -= HandleOnUpdateEnemyHealth2D2;
        EnemyBehaviour2DSecond.OnUpdateEnemyHealth2D3 -= HandleOnUpdateEnemyHealth2D3;
        EnemyBehaviourBoss2D.OnUpdateEnemyHealthBoss2D -= HandleOnUpdateEnemyHealthBoss2D;
        BreakableBlockFunctionality.OnUpdateBlockHealth -= HandleOnUpdateBlockHealth;
        ItemCollision.OnCollide -= HandleOnCollide;
    }

    void Start () {
        playerHealthValue = 100;
        //pistolPlayer.SetActive(false);
        pistolSpotlight.GetComponent<Light>();
        pistolSpotlight.enabled = false;
        enemyHealthValue = 10;
        enemyHealthValue1 = 10;
        enemyHealthValue2 = 10;
        enemyHealthValue3 = 10;
        enemyHealthValueBoss = 25;
        pauseCanvas.SetActive(false);
        deathCanvas.SetActive(false);
        //levelAdvanceCanvas.SetActive(false);
        //levelResetCanvas.SetActive(false);
        endingCanvas1.SetActive(false);
        endingCanvas2.SetActive(false);
        endingCanvas3.SetActive(false);
        enemyHealthValue2D = 10;
        enemyHealthValueBoss2D = 35;
    }
	
	void Update () {
        playerHealthBar.value = playerHealthValue - health;

        if (GameObject.FindGameObjectWithTag("Player 2D") == null)
        {
            enemyHealthBar.value = enemyHealthValue - enemyHealth;
            enemyHealthBar1.value = enemyHealthValue1 - enemyHealth1;
            enemyHealthBar2.value = enemyHealthValue2 - enemyHealth2;
            enemyHealthBar3.value = enemyHealthValue3 - enemyHealth3;
            enemyHealthBarBoss.value = enemyHealthValueBoss - enemyHealthBoss;
        }

        if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            enemyHealthBar2D.value = enemyHealthValue2D - enemyHealth2D;
            enemyHealthBar2D2.value = enemyHealthValue2D2 - enemyHealth2D2;
            enemyHealthBar2D3.value = enemyHealthValue2D3 - enemyHealth2D3;
            enemyHealthBarBoss2D.value = enemyHealthValueBoss2D - enemyHealthBoss2D;
        }

        if (rocksExplosiveOne == null) //If the object isnt present the engine will return null
        {
            rockOne.constraints = RigidbodyConstraints.None;
            rockTwo.constraints = RigidbodyConstraints.None;
            rockThree.constraints = RigidbodyConstraints.None;
        }
        if (rocksExplosiveTwo == null) //If the object isnt present the engine will return null
        {
            rockFour.constraints = RigidbodyConstraints.None;
            rockFive.constraints = RigidbodyConstraints.None;
            rockSix.constraints = RigidbodyConstraints.None;
        }

        if (Input.GetKeyDown("escape"))
        {
            isVisible =! isVisible;
            pauseCanvas.SetActive(isVisible);
            Time.timeScale = 0.0000000001f;
        }
        else if (isVisible == false)
        {
            Time.timeScale = 1.0f;
        }

        if (pistolSpotlight.enabled == false && Input.GetKeyDown(KeyCode.C))
        {
            pistolSpotlight.enabled = true;
        }
        else if (pistolSpotlight.enabled == true && Input.GetKeyDown(KeyCode.C))
        {
            pistolSpotlight.enabled = false;
        }
    }

    void HandleOnUpdatePlayerHealth(int playerNewHealth)
    {
        health = playerNewHealth;

        if (playerHealthValue < 1)
        {
            Time.timeScale = 0.000001f;
            EnemyBehaviour.playerIsAlive = false;
            deathCanvas.SetActive(true);
        }
    }

    void HandleOnUpdateEnemyHealth(int enemyNewHealth)
    {
        enemyHealth = enemyNewHealth;
    }

    void HandleOnUpdateEnemyHealth1(int enemyNewHealth1)
    {
        enemyHealth1 = enemyNewHealth1;
    }

    void HandleOnUpdateEnemyHealth2(int enemyNewHealth2)
    {
        enemyHealth2 = enemyNewHealth2;
    }

    void HandleOnUpdateEnemyHealth3(int enemyNewHealth3)
    {
        enemyHealth3 = enemyNewHealth3;
    }

    void HandleOnUpdateEnemyHealthBoss(int enemyNewHealthBoss)
    {
        enemyHealthBoss = enemyNewHealthBoss;
    }

    void HandleOnUpdateEnemyHealth2D(int enemyNewHealth2D)
    {
        enemyHealth2D = enemyNewHealth2D;
    }

    void HandleOnUpdateEnemyHealth2D2(int enemyNewHealth2D2)
    {
        enemyHealth2D2 = enemyNewHealth2D2;
    }

    void HandleOnUpdateEnemyHealth2D3(int enemyNewHealth2D3)
    {
        enemyHealth2D3 = enemyNewHealth2D3;
    }

    void HandleOnUpdateEnemyHealthBoss2D(int enemyNewHealthBoss2D)
    {
        enemyHealthBoss2D = enemyNewHealthBoss2D;
    }

    void HandleOnUpdateBlockHealth(int blockNewHealth)
    {
        blockHealth = blockNewHealth;
    }

    void HandleOnCollide()
    {
        pistolPlayer.SetActive(true);
    }
}
