﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveAlongXAxis : MonoBehaviour {

    public float amplitude = 0.7f;
    public float frequency = 1f;
    Vector3 positionOffset = new Vector3();
    Vector3 temporaryPosition = new Vector3();

    void Start () {
        positionOffset = transform.position; //This logs the game object's starting position and rotation when the scene is loaded.
    }
	
	// Update is called once per frame
	void Update () {
        temporaryPosition = positionOffset;
        temporaryPosition.x += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;
        //This code is responsible for the sliding of the platforms on the X axis via the use of SIN.
        //The position offset in this case is the starting position so that the SIN function knows what the range of the slide is due to its starting position.
        //The frequency is the speed of the slide whereas the amplitude increases or decreases the range in which the platform can slide between on the X Axis.

        transform.position = temporaryPosition;
    }
}
