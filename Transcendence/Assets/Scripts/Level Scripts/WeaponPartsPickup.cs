﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponPartsPickup : MonoBehaviour {

    public bool weaponPart1Collected = false;
    public bool weaponPart2Collected = false;
    public bool weaponPart3Collected = false;
    public GameObject weaponPart1;
    public GameObject weaponPart2;
    public GameObject weaponPart3;
    public GameObject interactCanvas;
    private bool isVisible = false;
	
    void Start()
    {
        interactCanvas.SetActive(isVisible = false);
        PlayerPrefs.SetInt("Weapon Component 1", 0);
        PlayerPrefs.SetInt("Weapon Component 2", 0);
        PlayerPrefs.SetInt("Weapon Component 3", 0);
    }


    void OnTriggerEnter(Collider collide)
    {
        interactCanvas.SetActive(isVisible = true);
    }

    void OnTriggerStay (Collider collide)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 1")
        {
            weaponPart1Collected = true;
            Destroy(gameObject);
            interactCanvas.SetActive(isVisible = false);
            PlayerPrefs.SetInt("Weapon Component 1", 1);
            print("Player Prefs set to 1");
        }
        else if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 2")
        {
            weaponPart2Collected = true;
            Destroy(gameObject);
            interactCanvas.SetActive(isVisible = false);
            PlayerPrefs.SetInt("Weapon Component 2", 1);
        }
        else if (GameObject.FindGameObjectWithTag("Player") == null)
        {
            if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 3")
            {
                weaponPart3Collected = true;
                Destroy(gameObject);
                interactCanvas.SetActive(isVisible = false);
                PlayerPrefs.SetInt("Weapon Component 3", 1);
            }
        }
    }

    void OnTriggerExit(Collider collide)
    {
        interactCanvas.SetActive(isVisible = false);
    }
}
