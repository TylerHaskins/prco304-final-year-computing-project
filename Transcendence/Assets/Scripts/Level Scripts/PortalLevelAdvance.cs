﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PortalLevelAdvance : MonoBehaviour {

    public GameObject levelAdvanceConfirmCanvas;
    private bool isVisible = false;
    public string levelName;

    void Start()
    {
        levelAdvanceConfirmCanvas.SetActive(isVisible);
    }

    void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Player within portal trigger");
        if (collision.gameObject.tag == "Player")
        {
            levelAdvanceConfirmCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Player within portal trigger");
        if (collision.gameObject.tag == "Player 2D")
        {
            levelAdvanceConfirmCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.Q))
        {
            levelAdvanceConfirmCanvas.SetActive(isVisible = false);
            SceneManager.LoadScene(levelName);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            levelAdvanceConfirmCanvas.SetActive(isVisible = false);
            SceneManager.LoadScene(levelName);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        Debug.Log("Player exited portal trigger");
        levelAdvanceConfirmCanvas.SetActive(isVisible = false);
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        levelAdvanceConfirmCanvas.SetActive(isVisible = false);
    }
}
