﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBehaviour : MonoBehaviour {

    public delegate void UpdateExplosiveHealth(int newExplosiveHealth);
    public static event UpdateExplosiveHealth OnUpdateExplosiveHealth;

    public int explosiveHealth = 6;
    public string damageTag = "";
    public float explosionRadius = 5.0f;
    public float explosionPower = 10.0f;


	// Use this for initialization
	void Start () {
        SendExplosiveHealthData();
	}
	
    void SendExplosiveHealthData()
    {
        if (OnUpdateExplosiveHealth != null)
        {
            OnUpdateExplosiveHealth(explosiveHealth);
        }
    }

    private void InflictDamageOnExplosive(int damage)
    {
        explosiveHealth -= damage;
        SendExplosiveHealthData();

        if (explosiveHealth <= 1)
        {
            Explosion();
            Destroy(gameObject);
        }
    }

    void Explosion()
    {
        Vector3 explosionPosition = transform.position;
        Collider[] objectColliders = Physics.OverlapSphere(explosionPosition, explosionRadius);
        foreach(Collider hit in objectColliders)
        {
            Rigidbody rigidbodyObstacle = hit.GetComponent<Rigidbody>();

            if (rigidbodyObstacle != null)
            {
                rigidbodyObstacle.AddExplosionForce(explosionPower, explosionPosition, explosionRadius, 3.0f);
            }
        }
    }
}
