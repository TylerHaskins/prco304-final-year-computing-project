﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MakeChild : MonoBehaviour {

    public Rigidbody playerCharacter;

	void OnTriggerEnter (Collider parentCollision)
    {
        if (parentCollision.gameObject.tag == "Player")
        {
            //playerCharacter.constraints = RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezeRotationX;
            parentCollision.transform.parent = transform;
            parentCollision.transform.rotation = Quaternion.Euler(0, parentCollision.transform.eulerAngles.y, 0);
        }
    }

    void OnTriggerExit(Collider parentCollision)
    {
        if (parentCollision.gameObject.tag == "Player")
        {
            playerCharacter.constraints = RigidbodyConstraints.None;
            parentCollision.transform.parent = null;
        }
    }
}
