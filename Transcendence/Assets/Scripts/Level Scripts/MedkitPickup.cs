﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MedkitPickup : MonoBehaviour {

    public int restoreHealth = 25;
    public GameObject interactCanvas;
    private bool isVisible = false;

    void Start()
    {
        interactCanvas.SetActive(isVisible);
    }

    void OnTriggerEnter (Collider other)
    {
        interactCanvas.SetActive(isVisible = true);
    }

    void OnTriggerStay (Collider other)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && other.gameObject.tag == "Player")
        {
            other.SendMessage("RestoreHealth", restoreHealth);
            Destroy(gameObject);
            interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        interactCanvas.SetActive(isVisible = false);
    }
}
