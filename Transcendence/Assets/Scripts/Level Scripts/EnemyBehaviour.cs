﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour {

    public delegate void UpdateEnemyHealth(int newEnemyHealth);
    public static event UpdateEnemyHealth OnUpdateEnemyHealth;
    public delegate void UpdateEnemyHealth1(int newEnemyHealth1);
    public static event UpdateEnemyHealth1 OnUpdateEnemyHealth1;
    public delegate void UpdateEnemyHealth2(int newEnemyHealth2);
    public static event UpdateEnemyHealth2 OnUpdateEnemyHealth2;
    public delegate void UpdateEnemyHealth3(int newEnemyHealth3);
    public static event UpdateEnemyHealth3 OnUpdateEnemyHealth3;
    public delegate void UpdateEnemyHealthBoss(int newEnemyHealthBoss);
    public static event UpdateEnemyHealthBoss OnUpdateEnemyHealthBoss;

    public int enemyHealth = 10;
    public int enemyHealth1 = 10;
    public int enemyHealth2 = 10;
    public int enemyHealth3 = 10;
    public int enemyHealthBoss = 25;
    public int enemyDamage = 10;
    public float playerDistance;
    public string damageThePlayerTag = "";
    public static bool playerIsAlive = true;
    public bool playerInRadius = false;
    public int damage = 10;
    public float force = 2f;

    public GameObject enemyBullet;
    public Transform player;
    private Transform _transform;


    // Use this for initialization
    void Start () {
        SendEnemyHealthData();
        SendEnemyHealthData1();
        SendEnemyHealthData2();
        SendEnemyHealthData3();
        SendEnemyHealthDataBoss();
        _transform = transform;
    }

    void Update()
    {
        if (playerIsAlive == true)
        {
            playerDistance = Vector3.Distance(player.position, transform.position);

            if (playerDistance < 15f)
            {
                playerInRadius = true;
                LookAtPlayer();
                AttackPlayer();
            }
        }
    }
	
	void SendEnemyHealthData()
    {
        if(OnUpdateEnemyHealth != null)
        {
            OnUpdateEnemyHealth(enemyHealth);
        }
    }

    void SendEnemyHealthData1()
    {
        if (OnUpdateEnemyHealth1 != null)
        {
            OnUpdateEnemyHealth1(enemyHealth1);
        }
    }

    void SendEnemyHealthData2()
    {
        if (OnUpdateEnemyHealth2 != null)
        {
            OnUpdateEnemyHealth2(enemyHealth2);
        }
    }

    void SendEnemyHealthData3()
    {
        if (OnUpdateEnemyHealth3 != null)
        {
            OnUpdateEnemyHealth3(enemyHealth3);
        }
    }

    void SendEnemyHealthDataBoss()
    {
        if (OnUpdateEnemyHealthBoss != null)
        {
            OnUpdateEnemyHealthBoss(enemyHealthBoss);
        }
    }

    private void InflictDamageOnEnemy(int damage)
    {
        enemyHealth -= damage;
        SendEnemyHealthData();

        if(enemyHealth <= 1)
        {
            Destroy(gameObject);
        }
    }

    private void InflictDamageOnEnemy1(int damage)
    {
        enemyHealth1 -= damage;
        SendEnemyHealthData1();

        if (enemyHealth1 <= 1)
        {
            Destroy(gameObject);
        }
    }

    private void InflictDamageOnEnemy2(int damage)
    {
        enemyHealth2 -= damage;
        SendEnemyHealthData2();

        if (enemyHealth2 <= 1)
        {
            Destroy(gameObject);
        }
    }

    private void InflictDamageOnEnemy3(int damage)
    {
        enemyHealth3 -= damage;
        SendEnemyHealthData3();

        if (enemyHealth3 <= 1)
        {
            Destroy(gameObject);
        }
    }

    private void InflictDamageOnEnemyBoss(int damage)
    {
        enemyHealthBoss -= damage;
        SendEnemyHealthDataBoss();

        if (enemyHealthBoss <= 1)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.CompareTag(damageThePlayerTag))
        {
            collision.SendMessage("TakeDamage", enemyDamage);
        }
    }

    void LookAtPlayer()
    {
        transform.LookAt(player);
    }
    
    void AttackPlayer()
    {
        if (GameObject.FindWithTag("Enemy Bullet") == null)
        {
            Instantiate(enemyBullet, transform.position + transform.forward * 3, transform.rotation);
        }
    }

}
