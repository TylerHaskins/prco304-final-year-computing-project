﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformActivater : MonoBehaviour {

    public GameObject entryPlatform;
    public GameObject exitPlatform;
    public GameObject volcanoCollectable;

	void Update () {
		if (volcanoCollectable == null)
        {
            entryPlatform.SetActive(true);
            exitPlatform.SetActive(true);
        }
	}

    void OnTriggerEnter(Collider collide)
    {
        if(collide.tag == "Player")
        {
            entryPlatform.SetActive(false);
            exitPlatform.SetActive(false);
        }
    }
}
