﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelReset : MonoBehaviour {

    public GameObject levelResetConfirmCanvas;
    private bool isVisible = false;
    public string levelName;

    void Start()
    {
        levelResetConfirmCanvas.SetActive(isVisible);
    }

    void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Player within portal trigger");
        if (collision.gameObject.tag == "Player")
        {
            levelResetConfirmCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.A))
        {
            levelResetConfirmCanvas.SetActive(isVisible = false);
            SceneManager.LoadScene(levelName);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        Debug.Log("Player exited portal trigger");
        levelResetConfirmCanvas.SetActive(isVisible = false);
    }
}
