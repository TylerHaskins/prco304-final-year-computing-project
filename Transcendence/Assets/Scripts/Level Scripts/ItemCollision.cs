﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemCollision : MonoBehaviour {

    public delegate void ItemCollide();
    public static event ItemCollide OnCollide;

    public GameObject interactCanvas;
    public GameObject endingCanvas1;
    public GameObject endingCanvas2;
    public GameObject endingCanvas3;
    private bool isVisible = false;

    private int weaponPartOnePrefs;
    private int weaponPartTwoPrefs;
    private int weaponPartThreePrefs;

    void Start()
    {
        interactCanvas.SetActive(isVisible);
        SetPrefs();
    }

    void SetPrefs()
    {
        weaponPartOnePrefs = PlayerPrefs.GetInt("Weapon Component 1");
        weaponPartTwoPrefs = PlayerPrefs.GetInt("Weapon Component 2");
        weaponPartThreePrefs = PlayerPrefs.GetInt("Weapon Component 3");
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            interactCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player 2D")
        {
            interactCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerStay(Collider collision)
    {
        //if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        //{
        //    OnCollide();
        //    Destroy(gameObject);
        //    interactCanvas.SetActive(isVisible = false);
        //}

        //Only one weapon part picked up
        if (weaponPartOnePrefs == 1 && weaponPartTwoPrefs == 0 && weaponPartThreePrefs == 0 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas1.SetActive(true);
        }
        else if (weaponPartOnePrefs == 0 && weaponPartTwoPrefs == 1 && weaponPartThreePrefs == 0 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas1.SetActive(true);
        }
        else if (weaponPartOnePrefs == 0 && weaponPartTwoPrefs == 0 && weaponPartThreePrefs == 1 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas1.SetActive(true);
        }

        //Only two weapon parts picked up
        else if (weaponPartOnePrefs == 1 && weaponPartTwoPrefs == 1 && weaponPartThreePrefs == 0 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas2.SetActive(true);
        }
        else if (weaponPartOnePrefs == 0 && weaponPartTwoPrefs == 1 && weaponPartThreePrefs == 1 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas2.SetActive(true);
        }
        else if (weaponPartOnePrefs == 1 && weaponPartTwoPrefs == 0 && weaponPartThreePrefs == 1 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas2.SetActive(true);
        }

        //All three weapon parts picked up
        else if (weaponPartOnePrefs == 1 && weaponPartTwoPrefs == 1 && weaponPartThreePrefs == 1 && isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            endingCanvas3.SetActive(true);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player 2D")
        {
            OnCollide();
            Destroy(gameObject);
            interactCanvas.SetActive(isVisible = false);
        }
    }

        void OnTriggerExit(Collider collision)
    {
        interactCanvas.SetActive(isVisible = false);
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        interactCanvas.SetActive(isVisible = false);
    }
}
