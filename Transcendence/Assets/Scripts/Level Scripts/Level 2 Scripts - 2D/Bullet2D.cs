﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullet2D : MonoBehaviour {

    private Rigidbody2D bulletPrefab;
    public int bulletDamage = 2;
    public string bulletDamageTagPlayer = "";
    public string bulletDamageTagBlock = "";
    public float speed = 5.0f;
    public float destroyTime = 0.7f;

        void Start()
        {
        Invoke("Die", destroyTime);
        }
        void OnTriggerEnter2D(Collider2D bulletCollide)
        {
            if (bulletCollide.CompareTag("Enemy 1") || bulletCollide.CompareTag("Enemy 2") || bulletCollide.CompareTag("Enemy 3") || bulletCollide.CompareTag("Enemy Boss"))
            {
                Debug.Log("Bullet Impacted Enemy");
                bulletCollide.SendMessage("InflictDamageOnEnemy", bulletDamage);
            }
            else if (bulletCollide.CompareTag(bulletDamageTagBlock))
            {
                Debug.Log("Bullet Impacted Block");
                bulletCollide.SendMessage("InflictDamageOnBlock", bulletDamage);
            }
            //else if (bulletCollide.CompareTag(bulletDamageTagPlayer))
            //{
            //    Debug.Log("Bullet Impacted Player");
            //    bulletCollide.SendMessage("TakeDamage", bulletDamage);
            //}
        }

        void Die()
        {
        Destroy(gameObject);
        }
        void OnDestroy()
        {
        CancelInvoke("Die");
        }
        void FixedUpdate()
        {
        GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
	    }
}
