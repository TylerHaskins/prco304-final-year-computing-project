﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootBullet2D : MonoBehaviour
{

    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Text ammunitionClipCount;
    public AudioSource bulletFireSoundEffect;
    public AudioSource emptyClipFireSound;
    public AudioSource pistolReloadSound;
    public float fireTime = 0.5f;
    public int bulletCount;
    private int clipSize = 15;
    public int damage = 5;
    public int speed = 5;
    public string damageTag = "";

    private bool isFiring = false;
    private bool shootingEnabled = false;

    void Start()
    {
        ammunitionClipCount.text = "0/0";
        shootingEnabled = true;
        bulletCount = 15;
    }

    void SetFiring()
    {
        isFiring = false;
    }

    void Fire()
    {
        isFiring = true;
        Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 playerPosition = new Vector2(transform.position.x, transform.position.y + 1);
        Vector2 direction = target - playerPosition;
        direction.Normalize();
       //GameObject playerBullet = Instantiate(bulletPrefab, playerPosition, Quaternion.identity);
        //playerBullet.GetComponent<Rigidbody2D>().velocity = direction * speed;
        Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bulletFireSoundEffect.Play();
        bulletCount--;
        UpdateAmmunitionText();
        Invoke("SetFiring", fireTime);
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && bulletCount > 0)
        {
            shootingEnabled = true;
            if (!isFiring)
            {
                Fire();
            }
        }
        else if (bulletCount <= 0 && Input.GetMouseButtonDown(0))
        {
            shootingEnabled = false;
            emptyClipFireSound.Play();
        }
        ReloadPistol();
    }


    void ReloadPistol()
    {
        if (bulletCount <= 0 && clipSize >= 15 && Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload method called");
            shootingEnabled = false;
            Debug.Log("Shooting Disabled");
            bulletCount = clipSize;
            pistolReloadSound.Play();
            clipSize -= 15;
            UpdateAmmunitionText();
        }
        else if (clipSize < 1 && Input.GetKeyDown(KeyCode.R) || bulletCount <= 1 && clipSize <= 1 && Input.GetKeyDown(KeyCode.R))
        {
            emptyClipFireSound.Play();
        }
    }

    void UpdateAmmunitionText()
    {
        shootingEnabled = true;
        Debug.Log("Shooting Enabled");
        ammunitionClipCount.text = bulletCount + "/" + clipSize;
    }
}
