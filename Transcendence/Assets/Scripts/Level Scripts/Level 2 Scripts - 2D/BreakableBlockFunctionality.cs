﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableBlockFunctionality : MonoBehaviour {

    public delegate void UpdateBlockHealth(int newBlockHealth);
    public static event UpdateBlockHealth OnUpdateBlockHealth;

    public int blockHealth = 5;

    void Start()
    {
        SendBlockHealthData();
    }

    void SendBlockHealthData()
    {
        if (OnUpdateBlockHealth != null)
        {
            OnUpdateBlockHealth(blockHealth);
        }
    }

    private void InflictDamageOnBlock(int damage)
    {
        blockHealth -= damage;
        SendBlockHealthData();

        if (blockHealth <= 1)
        {
            Destroy(gameObject);
        }
    }
}
