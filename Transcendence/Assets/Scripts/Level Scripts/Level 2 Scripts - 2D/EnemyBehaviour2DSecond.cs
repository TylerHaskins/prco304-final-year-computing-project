﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour2DSecond : MonoBehaviour
{
    public delegate void UpdateEnemyHealth2D3(int newEnemyHealth2D3);
    public static event UpdateEnemyHealth2D3 OnUpdateEnemyHealth2D3;

    public int enemyHealth2D3 = 10;
    public int enemyDamage = 10;
    public float playerDistance;
    public string damageThePlayerTag = "";
    public static bool playerIsAlive = true;
    public bool playerInRadius = false;
    public bool enemyAlive = true;
    public int damage = 10;
    public float force = 2f;

    public bool playerRight = false;
    public bool playerLeft = false;

    public GameObject enemyBullet;
    public Transform player;
    private Transform _transform;


    // Use this for initialization
    void Start()
    {
        SendEnemyHealthData2D3();
        _transform = transform;
    }

    void Update()
    {
        if (playerIsAlive == true)
        {
            playerDistance = Vector3.Distance(player.position, transform.position);

            if (playerDistance < 20f)
            {
                playerInRadius = true;

                if (player.transform.position.x > transform.position.x)
                {
                    print("Player to right");
                    playerLeft = false;
                    playerRight = true;
                }
                else if (player.transform.position.x < transform.position.x)
                {
                    print("Player to left");
                    playerLeft = true;
                    playerRight = false;
                }

                AttackPlayer();
            }
        }
    }

    void SendEnemyHealthData2D3()
    {
        if (OnUpdateEnemyHealth2D3 != null)
        {
            OnUpdateEnemyHealth2D3(enemyHealth2D3);
        }
    }

    private void InflictDamageOnEnemy(int damage)
    {
        enemyHealth2D3 -= damage;
        SendEnemyHealthData2D3();

        if (enemyHealth2D3 <= 1)
        {
            enemyAlive = false;
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(damageThePlayerTag))
        {
            collision.SendMessage("TakeDamage", enemyDamage);
        }
    }

    void AttackPlayer()
    {
        if (GameObject.FindWithTag("Enemy Bullet") == null)
        {
            if (playerLeft == true)
            {
                Instantiate(enemyBullet, transform.position + -transform.right * 3, transform.rotation);
            }

            else if (playerRight == true)
            {
                Instantiate(enemyBullet, transform.position + transform.right * 3, transform.rotation);
            }
        }
    }

}
