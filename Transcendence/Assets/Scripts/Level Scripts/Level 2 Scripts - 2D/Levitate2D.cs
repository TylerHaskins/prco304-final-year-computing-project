﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Levitate2D : MonoBehaviour {

    public float amplitude = 0.7f;
    public float frequency = 1f;
    Vector2 positionOffset = new Vector2();
    Vector2 temporaryPosition = new Vector2();

    // Use this for initialization
    void Start()
    {
        positionOffset = transform.position; //This logs the game object's starting position and rotation when the scene is loaded.
    }

    // Update is called once per frame
    void Update()
    {
        temporaryPosition = positionOffset;
        temporaryPosition.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = temporaryPosition;
    }
}
