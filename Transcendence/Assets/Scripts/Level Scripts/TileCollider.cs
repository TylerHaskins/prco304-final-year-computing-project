﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class OnHitTileEvent : UnityEvent<int> { }

public class TileCollider : MonoBehaviour {

    public int index;
    public OnHitTileEvent onHit;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
        onHit.Invoke(index);
	}
}
