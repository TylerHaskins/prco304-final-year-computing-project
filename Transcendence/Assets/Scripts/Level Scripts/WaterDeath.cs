﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDeath : MonoBehaviour {

    public GameObject deathPanel;
    public GameObject playerController;
    //public AudioSource waterSplashSound;
    public bool deathPanelIsVisible = false;

    void Start()
    {
        deathPanel.SetActive(deathPanelIsVisible);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == playerController)
        {
            //waterSplashSound.Play();
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject == playerController)
        {
            Debug.Log("Player within trigger");
            deathPanel.SetActive(deathPanelIsVisible = true);
            Time.timeScale = 0.0000000001f;
        }
    }
}
