﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PistolFire : MonoBehaviour {

    public GameObject bullet;
    private Transform _transform;
    public int damage = 5;
    public string damageTag = "";
    public float force = 5f;
    public AudioSource bulletFireSoundEffect;
    public AudioSource emptyClipFireSound;
    public AudioSource pistolReloadSound;
    public int bulletCount;
    private int clipSize;
    private int maxSingleClipSize;
    private int maxOverallClipSize;
    private bool shootingEnabled = true;
    private bool ammoIncreaseOnce = false;
    public Text ammunitionClipCount;

	void Start () {
        _transform = transform;
        ammunitionClipCount.text = "0/0";
        shootingEnabled = false;
        clipSize = 0;
        bulletCount = 0;
        maxSingleClipSize = 15;
        maxOverallClipSize = 45;
        Debug.Log("Shooting Disabled");
    }

    //void OnEnable()
    //{
    //    ItemCollision.OnCollide += HandleOnItemCollide;
    //}

    //void OnDisable()
    //{
    //    ItemCollision.OnCollide -= HandleOnItemCollide;
    //}
	
	void Update () {
        if (Input.GetMouseButtonDown(0) && shootingEnabled == true)
        {
            if (bulletCount > 0)
            {
                bulletFireSoundEffect.Play();
                Instantiate(bullet, transform.position + transform.forward * 3, transform.rotation);
                bulletCount--;
                UpdateAmmunitionText();
            }
            else if (bulletCount <= 0 && Input.GetMouseButtonDown(0))
            {
                emptyClipFireSound.Play();
            }
        }

        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 1") != null && GameObject.FindGameObjectWithTag("Ammo Clip 1").GetComponent<AmmoCollectable>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }

        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 2") != null && GameObject.FindGameObjectWithTag("Ammo Clip 2").GetComponent<AmmoCollectable2>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }

        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 3") != null && GameObject.FindGameObjectWithTag("Ammo Clip 3").GetComponent<AmmoCollectable3>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }
        ReloadPistol();
	}

    void MakeAmmoIncreaseFalse()
    {
        ammoIncreaseOnce = false;
    }

    void ReloadPistol()
    {
         if (bulletCount <= 0 && clipSize >= 15 && Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload method called");
            shootingEnabled = false;
            Debug.Log("Shooting Disabled");
            bulletCount += 15;
            pistolReloadSound.Play();
            clipSize -= 15;
            UpdateAmmunitionText();
        }
        else if (clipSize < 1 && Input.GetKeyDown(KeyCode.R) || bulletCount <= 1 && clipSize <= 1 && Input.GetKeyDown(KeyCode.R))
        {
            emptyClipFireSound.Play();
        }
    }

    void UpdateAmmunitionText()
    {
        shootingEnabled = true;
        Debug.Log("Shooting Enabled");
        ammunitionClipCount.text = bulletCount + "/" + clipSize;
    }

    //void HandleOnItemCollide()
    //{
    //    shootingEnabled = true;
    //    Debug.Log("Shooting Enabled");
    //    bulletCount = clipSize;
    //    ammunitionClipCount.text = bulletCount + "/" + clipSize;

    //    if (gameObject.tag == "Ammo Clip")
    //    {
    //        clipSize += 15;
    //    }
    //}
}
