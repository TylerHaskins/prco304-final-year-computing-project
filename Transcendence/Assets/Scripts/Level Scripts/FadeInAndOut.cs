﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeInAndOut : MonoBehaviour {

    public float alphaFadeOutValue = 1.0f;
    public Texture blackTexture;

    void OnGUI()
    {
        alphaFadeOutValue -= Mathf.Clamp01(Time.deltaTime / 13); //Time it takes for the texture to fade out completely to reveal the game world.
        GUI.color = new Color(0, 0, 0, alphaFadeOutValue);
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), blackTexture); //Make sure the black texture fills the whole of the screen regardless of the size.
    }
}
