﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletFire : MonoBehaviour {

    public float bulletSpeed = 0.5f;
    public float destroyBulletAfter = 2.5f;

	// Use this for initialization
	void Start () {
        Invoke("Disable", destroyBulletAfter);
	}

    void Disable()
    {
        Destroy(gameObject);
    }

    void OnDisable()
    {
        CancelInvoke("Disable");
    }
	
	// Update is called once per frame
	void Update () {
        GetComponent <Rigidbody>().velocity = transform.forward * bulletSpeed;
	}
}
