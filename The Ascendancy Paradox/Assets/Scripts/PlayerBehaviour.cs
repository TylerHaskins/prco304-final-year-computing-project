﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {

    public delegate void UpdatePlayerHealth(int newPlayerHealth);
    public static event UpdatePlayerHealth OnUpdatePlayerHealth;

    public int health = 100;
    public AudioSource jumpSound;

    //public GameObject playerSpawnPointObject; //Spawn point game object.
    private Vector3 playerSpawnPoint; //Coordinates of the spawn point in the game world.

    public GameObject stoneOne;
    public GameObject stoneTwo;
    public GameObject stoneThree;
    public GameObject stoneFour;
    public GameObject stoneFive;
    public GameObject stoneSix;
    public GameObject stoneSeven;
    public GameObject buttonOne;
    public GameObject buttonTwo;
    public GameObject buttonThree;
    public GameObject buttonFour;
    public GameObject buttonFive;
    public GameObject buttonSix;
    public GameObject buttonSeven;
    public GameObject dataPadOneCanvas;
    public GameObject dataPadTwoCanvas;

    public GameObject isLevel2DIdentifier;

    public GameObject interactCanvas;
    public GameObject deathCanvas;

    public GameObject wallPuzzleGate;
    public GameObject path1Gate;
    public GameObject timeGate;
    public GameObject deathGate;
    public GameObject riddleConsole;
    public GameObject weaponPart1;
    public GameObject weaponPart2;
    public GameObject weaponPart3;

    public bool isVisible = false;
    public bool stoneVisible = false;
    public bool allStonesActive = false;
    private bool stoneOneVisible = false;
    private bool stoneTwoVisible = false;
    private bool stoneThreeVisible = false;
    private bool stoneFourVisible = false;
    private bool stoneFiveVisible = false;
    private bool stoneSixVisible = false;
    private bool stoneSevenVisible = false;
    private bool pathOneRespawn1Active = false;
    private bool pathOneRespawn2Active = false;
    private bool pathOneRespawn3Active = false;
    private bool pathTwoRespawn1Active = false;
    private bool pathTwoRespawn2Active = false;
    private bool pathTwoRespawn3Active = false;
    private bool dataPadIsVisible = false;
    public bool weaponPart1Collected = false;
    public bool weaponPart2Collected = false;
    public bool weaponPart3Collected = false;

    void Start () {
        //if (isLevel2DIdentifier == null)
        //{
        //    playerSpawnPoint = new Vector3(201, 23, 61); //The inital player spawn point on load.
        //    transform.position = playerSpawnPoint;
        //}
        SendPlayerHealthData();
        dataPadOneCanvas.SetActive(dataPadIsVisible);
        dataPadTwoCanvas.SetActive(dataPadIsVisible);
        stoneSeven.SetActive(stoneSevenVisible);
        PlayerPrefs.GetInt("Weapon Component 1");
        PlayerPrefs.GetInt("Weapon Component 2");
        PlayerPrefs.GetInt("Weapon Component 3");
        stoneOne.SetActive(stoneOneVisible = true);
        stoneTwo.SetActive(stoneTwoVisible = true);
        stoneThree.SetActive(stoneThreeVisible = true);
        stoneFour.SetActive(stoneFourVisible = true);
        stoneFive.SetActive(stoneFiveVisible = true);
        stoneSix.SetActive(stoneSixVisible = true);
        buttonOne.SetActive(false);
        buttonTwo.SetActive(false);
        buttonThree.SetActive(false);
        buttonFour.SetActive(false);
        buttonFive.SetActive(false);
        buttonSix.SetActive(false);
        buttonSeven.SetActive(false);
    }

    void UpdatePlayerPrefsWeaponOne()
    {
        PlayerPrefs.SetInt("Weapon Component 1", 1);
    }
    void UpdatePlayerPrefsWeaponTwo()
    {
        PlayerPrefs.SetInt("Weapon Component 2", 1);
    }
    void UpdatePlayerPrefsWeaponThree()
    {
        PlayerPrefs.SetInt("Weapon Component 3", 1);
    }

    void Update()
    {
        PlayJumpSound();

        if (allStonesActive == true)
        {
            Destroy(wallPuzzleGate);
        }

        if (dataPadIsVisible == true && Input.GetKeyDown(KeyCode.Q))
        {
            dataPadOneCanvas.SetActive(dataPadIsVisible = false);
            dataPadTwoCanvas.SetActive(dataPadIsVisible = false);
        }
    }
	
    void SendPlayerHealthData()
    {
        if (OnUpdatePlayerHealth != null)
        {
            OnUpdatePlayerHealth(health); //Update the player health if they have taken damage. Function is called every time damage is dealt to the player.
        }
    }

    public void TakeDamage(int damage)
    {
        health -= damage; //Subtract the damage from the overall player health

        SendPlayerHealthData(); //Update the overall health of the player

        if (health <= 2)
        {
            deathCanvas.SetActive(true);
            print("Player Dead");
            EnemyBehaviour.playerIsAlive = false;
            Time.timeScale = 0.000001f;
        }
    }

    public void RestoreHealth (int newPlayerHealth)
    {
            health += newPlayerHealth;
            print("Health Restored");
            SendPlayerHealthData();
    }

    public void PlayJumpSound()
    {
        if (Input.GetKeyDown(KeyCode.Space) && GameObject.FindGameObjectWithTag("Player 2D") == null)
        {
            Debug.Log("Jump sound played");
            jumpSound.Play();
        }
        else if (GameObject.FindGameObjectWithTag("Player") == null && Input.GetKeyDown(KeyCode.W) && GameObject.FindGameObjectWithTag("Ground Checker").GetComponent<GroundCheck>().isGrounded == true)
        {
            Debug.Log("Jump sound played");
            jumpSound.Play();
        }
    }

    void OnTriggerEnter(Collider collide)
    {
        if (collide.tag == "Path 1 Respawn 1" || collide.tag == "Path 1 Respawn 2" || collide.tag == "Path 1 Respawn 3" || collide.tag == "Path 2 Respawn 1" || collide.tag == "Path 2 Respawn 2" || collide.tag == "Path 3 Respawn 3")
        {
            interactCanvas.SetActive(isVisible = false);
        }

        interactCanvas.SetActive(isVisible = true);
    }

    void OnTriggerEnter2D (Collider2D collide)
    {
        if (collide.tag == "Weapon Component 3")
        {
            interactCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerStay(Collider collide)
    {
        if (collide.tag == "Button Reset" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = false);
            stoneOne.SetActive(stoneOneVisible = false);
            stoneTwo.SetActive(stoneTwoVisible = false);
            stoneThree.SetActive(stoneThreeVisible = false);
            stoneFour.SetActive(stoneFourVisible = false);
            stoneFive.SetActive(stoneFiveVisible = false);
            stoneSix.SetActive(stoneSixVisible = false);
            allStonesActive = false;
            buttonOne.SetActive(true);
            buttonTwo.SetActive(true);
            buttonThree.SetActive(true);
            buttonFour.SetActive(true);
            buttonFive.SetActive(true);
            buttonSix.SetActive(true);
            buttonSeven.SetActive(true);
            Debug.Log("Reset Stones Button Pressed");
        }
        else if (collide.tag == "Button One" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = false);
            stoneOne.SetActive(stoneOneVisible = true);
            stoneTwo.SetActive(stoneTwoVisible = false);
            stoneThree.SetActive(stoneThreeVisible = true);
            stoneFour.SetActive(stoneFourVisible = false);
            stoneFive.SetActive(stoneFiveVisible = true);
            stoneSix.SetActive(stoneSixVisible = false);
            Debug.Log("Button One Pressed");
        }
        else if (collide.tag == "Button Two" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = true);
            stoneOne.SetActive(stoneOneVisible = false);
            stoneTwo.SetActive(stoneTwoVisible = true);
            stoneThree.SetActive(stoneThreeVisible = false);
            stoneFour.SetActive(stoneFourVisible = false);
            stoneFive.SetActive(stoneFiveVisible = true);
            stoneSix.SetActive(stoneSixVisible = false);
            Debug.Log("Button Two Pressed");
        }
        else if (collide.tag == "Button Three" && isVisible == true && stoneOneVisible == true && stoneThreeVisible == true && stoneFiveVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneOne.SetActive(stoneOneVisible = true);
            stoneThree.SetActive(stoneThreeVisible = true);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Three Pressed");
        }
        else if (collide.tag == "Button Three" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = false);
            stoneOne.SetActive(stoneOneVisible = true);
            stoneTwo.SetActive(stoneTwoVisible = false);
            stoneThree.SetActive(stoneThreeVisible = true);
            stoneFour.SetActive(stoneFourVisible = false);
            stoneFive.SetActive(stoneFiveVisible = false);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Three Pressed");
        }
        else if (collide.tag == "Button Four" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = false);
            stoneOne.SetActive(stoneOneVisible = false);
            stoneTwo.SetActive(stoneTwoVisible = true);
            stoneThree.SetActive(stoneThreeVisible = false);
            stoneFour.SetActive(stoneFourVisible = true);
            stoneFive.SetActive(stoneFiveVisible = false);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Four Pressed");
        }
        else if (collide.tag == "Button Five" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = true);
            stoneOne.SetActive(stoneOneVisible = false);
            stoneTwo.SetActive(stoneTwoVisible = false);
            stoneThree.SetActive(stoneThreeVisible = false);
            stoneFour.SetActive(stoneFourVisible = true);
            stoneFive.SetActive(stoneFiveVisible = true);
            stoneSix.SetActive(stoneSixVisible = false);
            Debug.Log("Buton Five Pressed");
        }
        else if (collide.tag == "Button Six" && isVisible == true && stoneOneVisible == true && stoneThreeVisible == true && stoneFiveVisible == true && stoneSixVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneOne.SetActive(stoneOneVisible = true);
            stoneTwo.SetActive(stoneTwoVisible = true);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Six Pressed");
        }
        else if (collide.tag == "Button Six" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = false);
            stoneOne.SetActive(stoneOneVisible = true);
            stoneTwo.SetActive(stoneTwoVisible = true);
            stoneThree.SetActive(stoneThreeVisible = false);
            stoneFour.SetActive(stoneFourVisible = false);
            stoneFive.SetActive(stoneFiveVisible = false);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Six Pressed");
        }
        else if (collide.tag == "Button Seven" && isVisible == true && stoneOneVisible == true && stoneTwoVisible == true && stoneThreeVisible == true && stoneFiveVisible == true && stoneSixVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = true);
            stoneThree.SetActive(stoneThreeVisible = true);
            stoneFour.SetActive(stoneFourVisible = true);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Seven Pressed");
        }
        else if (collide.tag == "Button Seven" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            stoneSeven.SetActive(stoneSevenVisible = true);
            stoneOne.SetActive(stoneOneVisible = false);
            stoneTwo.SetActive(stoneTwoVisible = false);
            stoneThree.SetActive(stoneThreeVisible = true);
            stoneFour.SetActive(stoneFourVisible = true);
            stoneFive.SetActive(stoneFiveVisible = false);
            stoneSix.SetActive(stoneSixVisible = true);
            Debug.Log("Button Seven Pressed");
        }
        else if (stoneOneVisible == true && stoneTwoVisible == true && stoneThreeVisible == true && stoneFourVisible == true && stoneFiveVisible == true && stoneSixVisible == true && stoneSevenVisible == true)
        {
            allStonesActive = true;
        }
        else if (collide.tag == "Data Pad 1" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            dataPadOneCanvas.SetActive(dataPadIsVisible = true);
        }
        else if (collide.tag == "Data Pad 2" && isVisible == true && Input.GetKeyDown(KeyCode.E))
        {
            dataPadTwoCanvas.SetActive(dataPadIsVisible = true);
        }
        else if (collide.tag == "Gate Shut")
        {
            timeGate.SetActive(true);
            deathGate.SetActive(true);
        }
        else if (collide.tag == "Reset Ending")
        {
            interactCanvas.SetActive(false);
        }
        else if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 1")
        {
            print("Player Prefs 1 set to 1");
            UpdatePlayerPrefsWeaponOne();
            weaponPart1Collected = true;
            Destroy(weaponPart1);
            interactCanvas.SetActive(isVisible = false);
        }
        else if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 2")
        {
            Debug.Log("Player Prefs 2 set to 1");
            UpdatePlayerPrefsWeaponTwo();
            weaponPart2Collected = true;
            Destroy(weaponPart2);
            interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerStay2D (Collider2D collide)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collide.gameObject.tag == "Weapon Component 3")
        {
            UpdatePlayerPrefsWeaponThree();
            weaponPart3Collected = true;
            Destroy(weaponPart3);
            interactCanvas.SetActive(isVisible = false);
            print("Player Prefs 3 set to 1");
        }
    }

    void OnTriggerExit(Collider collide)
    {
        interactCanvas.SetActive(isVisible = false);
    }

    void OnTriggerExit2D(Collider2D collide)
    {
        if (collide.tag == "Weapon Component 3")
        {
            interactCanvas.SetActive(isVisible = false);
        }
    }
}
