﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBounce : MonoBehaviour {

    Vector3 positionOffset = new Vector3();
    Vector3 temporaryPosition = new Vector3();
    public float amplitude = 0.7f;
    public float frequency = 1f;

    // Use this for initialization
    void Start () {
        positionOffset = transform.position; //This logs the game object's starting position and rotation when the scene is loaded.
    }

    // Update is called once per frame
    void Update () {
        temporaryPosition = positionOffset;
        temporaryPosition.y += Mathf.Sin(Time.fixedTime * Mathf.PI * frequency) * amplitude;

        transform.position = temporaryPosition;
    }
}
