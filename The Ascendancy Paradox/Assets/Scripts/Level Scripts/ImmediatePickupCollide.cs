﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImmediatePickupCollide : MonoBehaviour {

    public delegate void AmmoCollide();
    public static event AmmoCollide OnCollide;
    public delegate void AmmoCollide2();
    public static event AmmoCollide2 OnCollide2;
    public delegate void AmmoCollide3();
    public static event AmmoCollide3 OnCollide3;

    public GameObject pistolPlayer;
    public GameObject pistolPickup;

    void Start()
    {
        pistolPlayer.SetActive(false);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            pistolPlayer.SetActive(true);
            Destroy(pistolPickup);
        }        
    }
}
