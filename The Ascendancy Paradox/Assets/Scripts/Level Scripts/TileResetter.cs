﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TileResetter : MonoBehaviour {

    public UnityEvent onReset;

    public Color resetColour = Color.grey;

    public GameObject tileOne;
    public GameObject tileTwo;
    public GameObject tileThree;
    public GameObject tileFour;
    public GameObject tileFive;
    public GameObject tileSix;
    public GameObject tileSeven;
    public GameObject tileEight;
    public GameObject tileNine;
    public GameObject resetTile;

    private bool tileOneActive = false;
    private bool tileTwoActive = false;
    private bool tileThreeActive = false;
    private bool tileFourActive = false;
    private bool tileFiveActive = false;
    private bool tileSixActive = false;
    private bool tileSevenActive = false;
    private bool tileEightActive = false;
    private bool tileNineActive = false;
    private bool allTilesActive = false;

    // Use this for initialization
    void Start () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        onReset.Invoke();
        if (other.gameObject.tag == "Player" && gameObject.tag == "Reset Tile")
        {
            Debug.Log("Reset Tile Activated");
            tileOne.GetComponent<Renderer>().material.color = resetColour;
            tileTwo.GetComponent<Renderer>().material.color = resetColour;
            tileThree.GetComponent<Renderer>().material.color = resetColour;
            tileFour.GetComponent<Renderer>().material.color = resetColour;
            tileFive.GetComponent<Renderer>().material.color = resetColour;
            tileSix.GetComponent<Renderer>().material.color = resetColour;
            tileSeven.GetComponent<Renderer>().material.color = resetColour;
            tileEight.GetComponent<Renderer>().material.color = resetColour;
            tileNine.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = false;
            tileFiveActive = false;
            tileSixActive = false;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = false;
            allTilesActive = false;

            tileOne.GetComponent<TileEvent>().tileOneActive = false;
            tileOne.GetComponent<TileEvent>().tileTwoActive = false;
            tileOne.GetComponent<TileEvent>().tileFourActive = false;
            tileTwo.GetComponent<TileEvent>().tileOneActive = false;
            tileTwo.GetComponent<TileEvent>().tileThreeActive = false;
            tileTwo.GetComponent<TileEvent>().tileFiveActive = false;
            tileThree.GetComponent<TileEvent>().tileTwoActive = false;
            tileThree.GetComponent<TileEvent>().tileSixActive = false;
            tileFour.GetComponent<TileEvent>().tileOneActive = false;
            tileFour.GetComponent<TileEvent>().tileFiveActive = false;
            tileFour.GetComponent<TileEvent>().tileSevenActive = false;
            tileFive.GetComponent<TileEvent>().tileFourActive = false;
            tileFive.GetComponent<TileEvent>().tileSixActive = false;
            tileSix.GetComponent<TileEvent>().tileThreeActive = false;
            tileSix.GetComponent<TileEvent>().tileFiveActive = false;
            tileSix.GetComponent<TileEvent>().tileNineActive = false;
            tileSeven.GetComponent<TileEvent>().tileFourActive = false;
            tileSeven.GetComponent<TileEvent>().tileEightActive = false;
            tileEight.GetComponent<TileEvent>().tileFiveActive = false;
            tileEight.GetComponent<TileEvent>().tileSevenActive = false;
            tileEight.GetComponent<TileEvent>().tileNineActive = false;
            tileNine.GetComponent<TileEvent>().tileSixActive = false;
            tileNine.GetComponent<TileEvent>().tileEightActive = false;
            tileNine.GetComponent<TileEvent>().tileNineActive = false;
        }
    }
}
