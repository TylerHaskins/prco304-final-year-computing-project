﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterDeath : MonoBehaviour {

    public GameObject deathPanel;
    public GameObject playerController;
    public AudioSource waterSplashSound;
    public bool deathPanelIsVisible = false;

    void Start()
    {
        deathPanel.SetActive(deathPanelIsVisible);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            waterSplashSound.Play();
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player 2D")
        {
            deathPanel.SetActive(deathPanelIsVisible = true);
            Time.timeScale = 0.0000000001f;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("Player within trigger");
            deathPanel.SetActive(deathPanelIsVisible = true);
            Time.timeScale = 0.0000000001f;
        }
    }
}
