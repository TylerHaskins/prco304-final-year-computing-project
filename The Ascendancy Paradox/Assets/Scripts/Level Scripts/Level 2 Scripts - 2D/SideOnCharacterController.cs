﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideOnCharacterController : MonoBehaviour
{
    public GroundCheck touchingGround;

    public float speed = 30.0f;
    public float jumpSpeed = 70.0f;
    new Rigidbody2D rigidbody2D;

    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        rigidbody2D.AddForce(new Vector2(Input.GetAxis("Horizontal") * speed, rigidbody2D.velocity.y), ForceMode2D.Force);
        if (Input.GetAxis("Horizontal") == 0)
        {
            rigidbody2D.velocity = new Vector2(0, rigidbody2D.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.W) && touchingGround.isGrounded)
        {
            rigidbody2D.AddForce(new Vector2(rigidbody2D.velocity.x, jumpSpeed), ForceMode2D.Impulse);
        }
    }
}
