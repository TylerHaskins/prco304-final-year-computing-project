﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet2D : MonoBehaviour
{
    private Rigidbody2D bulletPrefab;
    private Transform myTransform;
    public Transform playerPosition;
    public int bulletDamage = 5;
    public string bulletDamageTagPlayer = "";
    public float force = 1.0f;
    public int speed = 25;

    // Use this for initialization
    void Start()
    {
        bulletPrefab = GetComponent<Rigidbody2D>();
        myTransform = GetComponent<Transform>();
    }

    void Update()
    {
        if (GameObject.FindGameObjectWithTag("Enemy 1") != null && GameObject.FindGameObjectWithTag("Enemy 1").GetComponent<EnemyBehaviour2D>().enemyAlive == true)
        {
            CheckEnemyOne();
        }
        if (GameObject.FindGameObjectWithTag("Enemy 2") != null && GameObject.FindGameObjectWithTag("Enemy 2").GetComponent<EnemyBehav2D>().enemyAlive == true)
        {
            CheckEnemyTwo();
        }
        if (GameObject.FindGameObjectWithTag("Enemy 3") != null && GameObject.FindGameObjectWithTag("Enemy 3").GetComponent<EnemyBehaviour2DSecond>().enemyAlive == true)
        {
            CheckEnemyThree();
        }
        if (GameObject.FindGameObjectWithTag("Enemy Boss") != null && GameObject.FindGameObjectWithTag("Enemy Boss").GetComponent<EnemyBehaviourBoss2D>().enemyAlive == true)
        {
            CheckEnemyBoss();
        }

        Destroy(gameObject, 2.0f);
    }

    void OnTriggerEnter2D(Collider2D bulletCollide)
    {
        if (bulletCollide.CompareTag(bulletDamageTagPlayer))
        {
            Debug.Log("Bullet Impacted Player");
            bulletCollide.SendMessage("TakeDamage", bulletDamage);
        }
    }

    void CheckEnemyOne()
    {
        if (GameObject.FindGameObjectWithTag("Enemy 1").GetComponent<EnemyBehaviour2D>().playerLeft == true)
        {
            bulletPrefab.velocity = -transform.right * speed;
        }
        else if (GameObject.FindGameObjectWithTag("Enemy 1").GetComponent<EnemyBehaviour2D>().playerRight == true)
        {
            bulletPrefab.velocity = transform.right * speed;
        }
    }

    void CheckEnemyTwo()
    {
        if (GameObject.FindGameObjectWithTag("Enemy 2").GetComponent<EnemyBehav2D>().playerLeft == true)
        {
            bulletPrefab.velocity = -transform.right * speed;
        }
        else if (GameObject.FindGameObjectWithTag("Enemy 2").GetComponent<EnemyBehav2D>().playerRight == true)
        {
            bulletPrefab.velocity = transform.right * speed;
        }
    }

    void CheckEnemyThree()
    {
        if (GameObject.FindGameObjectWithTag("Enemy 3").GetComponent<EnemyBehaviour2DSecond>().playerLeft == true)
        {
            bulletPrefab.velocity = -transform.right * speed;
        }
        else if (GameObject.FindGameObjectWithTag("Enemy 3").GetComponent<EnemyBehaviour2DSecond>().playerRight == true)
        {
            bulletPrefab.velocity = transform.right * speed;
        }
    }

    void CheckEnemyBoss()
    {
        if (GameObject.FindGameObjectWithTag("Enemy Boss").GetComponent<EnemyBehaviourBoss2D>().playerLeft == true)
        {
            bulletPrefab.velocity = -transform.right * speed;
        }
        else if (GameObject.FindGameObjectWithTag("Enemy Boss").GetComponent<EnemyBehaviourBoss2D>().playerRight == true)
        {
            bulletPrefab.velocity = transform.right * speed;
        }
    }

}
