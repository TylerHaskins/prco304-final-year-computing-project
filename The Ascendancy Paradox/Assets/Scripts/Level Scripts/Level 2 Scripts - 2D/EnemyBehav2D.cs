﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehav2D : MonoBehaviour
{
    public delegate void UpdateEnemyHealth2D2(int newEnemyHealth2D2);
    public static event UpdateEnemyHealth2D2 OnUpdateEnemyHealth2D2;

    public int enemyHealth2D2 = 10;
    public int enemyDamage = 10;
    public float playerDistance;
    public string damageThePlayerTag = "";
    public static bool playerIsAlive = true;
    public bool playerInRadius = false;
    public bool enemyAlive = true;
    public int damage = 10;
    public float force = 2f;

    public bool playerRight = false;
    public bool playerLeft = false;

    public GameObject enemyBullet;
    public Transform player;
    private Transform _transform;

    public AudioSource enemyAttackSound;


    // Use this for initialization
    void Start()
    {
        SendEnemyHealthData2D2();
        _transform = transform;
    }

    void Update()
    {
        if (playerIsAlive == true)
        {
            playerDistance = Vector3.Distance(player.position, transform.position);

            if (playerDistance < 20f)
            {
                playerInRadius = true;

                if (player.transform.position.x > transform.position.x)
                {
                    print("Player to right");
                    playerLeft = false;
                    playerRight = true;
                }
                else if (player.transform.position.x < transform.position.x)
                {
                    print("Player to left");
                    playerLeft = true;
                    playerRight = false;
                }

                AttackPlayer();
            }
        }
    }

    void SendEnemyHealthData2D2()
    {
        if (OnUpdateEnemyHealth2D2 != null)
        {
            OnUpdateEnemyHealth2D2(enemyHealth2D2);
        }
    }

    private void InflictDamageOnEnemy(int damage)
    {
        enemyHealth2D2 -= damage;
        SendEnemyHealthData2D2();

        if (enemyHealth2D2 <= 1)
        {
            enemyAlive = false;
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(damageThePlayerTag))
        {
            collision.SendMessage("TakeDamage", enemyDamage);
        }
    }

    void AttackPlayer()
    {
        if (GameObject.FindWithTag("Enemy Bullet") == null)
        {
            if (playerLeft == true)
            {
                enemyAttackSound.Play();
                Instantiate(enemyBullet, transform.position + -transform.right * 3, transform.rotation);
            }

            else if (playerRight == true)
            {
                enemyAttackSound.Play();
                Instantiate(enemyBullet, transform.position + transform.right * 3, transform.rotation);
            }
        }
    }

}
