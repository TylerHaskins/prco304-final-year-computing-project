﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnGroundedAnimation : MonoBehaviour {

    public Sprite playerJumping;
    public Sprite playerGrounded;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameObject.FindGameObjectWithTag("Ground Checker").GetComponent<GroundCheck>().isGrounded == false)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = playerJumping;
        }
        else if (GameObject.FindGameObjectWithTag("Ground Checker").GetComponent<GroundCheck>().isGrounded == true)
        {
            this.gameObject.GetComponent<SpriteRenderer>().sprite = playerGrounded;
        }
    }
}
