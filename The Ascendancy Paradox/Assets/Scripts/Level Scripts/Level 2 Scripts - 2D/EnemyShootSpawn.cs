﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShootSpawn : MonoBehaviour {

    public float speed;
    public int bulletDamage = 5;
    public string damageThePlayerTag = "";
    public Rigidbody2D playerRigidbody;
    public GameObject enemyBullet;
    public bool playerIsInRange = false;
    public Transform launchPoint;
    public Transform playerTransform;
	
	// Update is called once per frame
	void Update () {
        if (playerIsInRange == true && GameObject.FindWithTag("Enemy Bullet") == null)
        {
            Instantiate(enemyBullet, launchPoint.position, launchPoint.rotation);
        }
	}

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.CompareTag(damageThePlayerTag))
        {
            other.SendMessage("TakeDamage", bulletDamage);
        }
    }
}
