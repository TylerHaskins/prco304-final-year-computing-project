﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootBullet2D : MonoBehaviour
{

    public GameObject bulletPrefab;
    public Transform bulletSpawn;
    public Text ammunitionClipCount;
    public AudioSource bulletFireSoundEffect;
    public AudioSource emptyClipFireSound;
    public AudioSource pistolReloadSound;
    public float fireTime = 0.5f;
    public int bulletCount;
    private int clipSize;
    private int maxSingleClipSize;
    private int maxOverallClipSize;
    public int damage = 5;
    public int speed = 5;
    public string damageTag = "";

    private bool isFiring = false;
    private bool shootingEnabled = false;
    private bool ammoIncreaseOnce = false;

    void Start()
    {
        ammunitionClipCount.text = "0/0";
        shootingEnabled = false;
        clipSize = 0;
        bulletCount = 0;
        maxSingleClipSize = 15;
        maxOverallClipSize = 45;
        Debug.Log("Shooting Disabled");
    }

    void SetFiring()
    {
        isFiring = false;
    }

    void Fire()
    {
        isFiring = true;
        Vector2 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 playerPosition = new Vector2(transform.position.x, transform.position.y + 1);
        Vector2 direction = target - playerPosition;
        direction.Normalize();
        Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        bulletFireSoundEffect.Play();
        bulletCount--;
        UpdateAmmunitionText();
        Invoke("SetFiring", fireTime);
    }

    void Update()
    {
        if (Input.GetMouseButton(0) && bulletCount > 0)
        {
            shootingEnabled = true;
            if (!isFiring)
            {
                Fire();
            }
        }
        else if (bulletCount <= 0 && Input.GetMouseButtonDown(0))
        {
            shootingEnabled = false;
            emptyClipFireSound.Play();
        }

        else if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 1") != null && GameObject.FindGameObjectWithTag("Ammo Clip 1").GetComponent<AmmoCollectable>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }

        else if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 2") != null && GameObject.FindGameObjectWithTag("Ammo Clip 2").GetComponent<AmmoCollectable2>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }

        else if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 3") != null && GameObject.FindGameObjectWithTag("Ammo Clip 3").GetComponent<AmmoCollectable3>().ammoPickedUp == true)
        {
            if (clipSize < maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }
        ReloadPistol();
    }


    void ReloadPistol()
    {
        if (bulletCount <= 0 && clipSize >= 15 && Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload method called");
            shootingEnabled = false;
            Debug.Log("Shooting Disabled");
            bulletCount = clipSize;
            pistolReloadSound.Play();
            clipSize -= 15;
            UpdateAmmunitionText();
        }
        else if (clipSize < 1 && Input.GetKeyDown(KeyCode.R) || bulletCount <= 1 && clipSize <= 1 && Input.GetKeyDown(KeyCode.R))
        {
            emptyClipFireSound.Play();
        }
    }

    void MakeAmmoIncreaseFalse()
    {
        ammoIncreaseOnce = false;
    }

    void UpdateAmmunitionText()
    {
        shootingEnabled = true;
        Debug.Log("Shooting Enabled");
        ammunitionClipCount.text = bulletCount + "/" + clipSize;
    }
}
