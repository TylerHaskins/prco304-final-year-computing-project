﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviourBoss2D : MonoBehaviour
{
    public delegate void UpdateEnemyHealthBoss2D(int newEnemyHealthBoss2D);
    public static event UpdateEnemyHealthBoss2D OnUpdateEnemyHealthBoss2D;

    public int enemyHealthBoss2D = 35;
    public int enemyDamage = 10;
    public float playerDistance;
    public string damageThePlayerTag = "";
    public static bool playerIsAlive = true;
    public bool playerInRadius = false;
    public bool enemyAlive = true;
    public int damage = 25;
    public float force = 2f;

    public bool playerRight = false;
    public bool playerLeft = false;

    public GameObject enemyBullet;
    public Transform player;
    private Transform _transform;

    public AudioSource enemyAttackSound;


    // Use this for initialization
    void Start()
    {
        SendEnemyHealthDataBoss2D();
        _transform = transform;
    }

    void Update()
    {
        if (playerIsAlive == true)
        {
            playerDistance = Vector3.Distance(player.position, transform.position);

            if (playerDistance < 20f)
            {
                playerInRadius = true;

                if (player.transform.position.x > transform.position.x)
                {
                    print("Player to right");
                    playerLeft = false;
                    playerRight = true;
                }
                else if (player.transform.position.x < transform.position.x)
                {
                    print("Player to left");
                    playerLeft = true;
                    playerRight = false;
                }

                AttackPlayer();
            }
        }
    }

    void SendEnemyHealthDataBoss2D()
    {
        if (OnUpdateEnemyHealthBoss2D != null)
        {
            OnUpdateEnemyHealthBoss2D(enemyHealthBoss2D);
        }
    }

    private void InflictDamageOnEnemy(int damage)
    {
        enemyHealthBoss2D -= damage;
        SendEnemyHealthDataBoss2D();

        if (enemyHealthBoss2D <= 1)
        {
            enemyAlive = false;
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(damageThePlayerTag))
        {
            collision.SendMessage("TakeDamage", enemyDamage);
        }
    }

    void AttackPlayer()
    {
        if (GameObject.FindWithTag("Enemy Bullet") == null)
        {
            if (playerLeft == true)
            {
                enemyAttackSound.Play();
                Instantiate(enemyBullet, transform.position + -transform.right * 3, transform.rotation);
            }

            else if (playerRight == true)
            {
                enemyAttackSound.Play();
                Instantiate(enemyBullet, transform.position + transform.right * 3, transform.rotation);
            }
        }
    }

}
