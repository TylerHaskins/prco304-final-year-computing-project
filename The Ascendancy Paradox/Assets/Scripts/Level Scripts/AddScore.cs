﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddScore : MonoBehaviour {

    public delegate void SendScore(int playerScore);
    public static event SendScore OnSendScore; //Enables the script to link with GameUI Handler for score

    public int score = 10; //Base score for killing an enemy

    public void RunSendScore()
    {
        if (OnSendScore != null)
        {
            OnSendScore(score); //Send the score to the GUI
        }
    }
}
