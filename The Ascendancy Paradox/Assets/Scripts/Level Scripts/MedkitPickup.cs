﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MedkitPickup : MonoBehaviour {

    public int restoreHealth = 25;
    public GameObject interactCanvas;
    public Slider playerHealthSlider;
    private bool isVisible = false;

    void Start()
    {
        interactCanvas.SetActive(isVisible);
    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            interactCanvas.SetActive(isVisible = true);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        interactCanvas.SetActive(isVisible = true);
    }

    void OnTriggerStay (Collider other)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && other.gameObject.tag == "Player")
        {
                other.SendMessage("RestoreHealth", restoreHealth);
                Destroy(gameObject);
                interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && other.gameObject.tag == "Player 2D")
        {
                other.SendMessage("RestoreHealth", restoreHealth);
                Destroy(gameObject);
                interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerExit2D(Collider2D other)
        {
            interactCanvas.SetActive(isVisible = false);
        }
    }
