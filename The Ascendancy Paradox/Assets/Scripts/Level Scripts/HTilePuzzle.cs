﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HTilePuzzle : MonoBehaviour {

    public GameObject tileOne;
    public GameObject tileTwo;
    public GameObject tileThree;
    public GameObject tileFour;
    public GameObject tileFive;
    public GameObject tileSix;
    public GameObject tileSeven;
    public GameObject tileEight;
    public GameObject tileNine;
    public GameObject resetBlock;
    public GameObject hTilePuzzleGate;

    public bool isActivated = false;

    public Color resetColour = Color.grey;
    public Color activatedColour = Color.green;

    public int[] sequence;
    public int current = 0;

    void Start()
    {
        resetBlock.GetComponent<BoxCollider>();
        tileOne.GetComponent<BoxCollider>();
        tileTwo.GetComponent<BoxCollider>();
        tileThree.GetComponent<BoxCollider>();
        tileFour.GetComponent<BoxCollider>();
        tileFive.GetComponent<BoxCollider>();
        tileSix.GetComponent<BoxCollider>();
        tileSeven.GetComponent<BoxCollider>();
        tileEight.GetComponent<BoxCollider>();
        tileNine.GetComponent<BoxCollider>();
    }

    void Update()
    {
        if (GameObject.Find("Tile 1").GetComponent<TileEvent>().tileOneActive == true && GameObject.Find("Tile 1").GetComponent<TileEvent>().tileTwoActive == true && GameObject.Find("Tile 1").GetComponent<TileEvent>().tileFourActive == true && GameObject.Find("Tile 4").GetComponent<TileEvent>().tileOneActive == true && GameObject.Find("Tile 4").GetComponent<TileEvent>().tileFiveActive == true && GameObject.Find("Tile 4").GetComponent<TileEvent>().tileSevenActive == true && GameObject.Find("Tile 7").GetComponent<TileEvent>().tileFourActive == true && GameObject.Find("Tile 7").GetComponent<TileEvent>().tileEightActive == true && GameObject.Find("Tile 5").GetComponent<TileEvent>().tileSixActive == true && GameObject.Find("Tile 5").GetComponent<TileEvent>().tileFourActive == true && GameObject.Find("Tile 6").GetComponent<TileEvent>().tileThreeActive == true && GameObject.Find("Tile 6").GetComponent<TileEvent>().tileFiveActive == true && GameObject.Find("Tile 6").GetComponent<TileEvent>().tileNineActive == true && GameObject.Find("Tile 3").GetComponent<TileEvent>().tileTwoActive == true && GameObject.Find("Tile 3").GetComponent<TileEvent>().tileSixActive == true && GameObject.Find("Tile 9").GetComponent<TileEvent>().tileSixActive == true && GameObject.Find("Tile 9").GetComponent<TileEvent>().tileEightActive == true && GameObject.Find("Tile 9").GetComponent<TileEvent>().tileNineActive == true)
        {
            Destroy(hTilePuzzleGate);
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileTwo.GetComponent<Renderer>().material.color = activatedColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileSix.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = activatedColour;
            tileEight.GetComponent<Renderer>().material.color = activatedColour;
            tileNine.GetComponent<Renderer>().material.color = activatedColour;
        }
    }

    public void OnReset()
    {
        current = 0;
    }
    }

