﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathTimeRiddleCollider : MonoBehaviour {

    public GameObject timeGate;
    public GameObject deathGate;
    public GameObject riddleConsole;
    public GameObject interactCanvas;
    private bool isVisible = false;

    void OnTriggerEnter (Collider collide)
    {
        interactCanvas.SetActive(isVisible = true);
    }

    void OnTriggerStay (Collider collide)
    {
        if (collide.tag == "Player" && Input.GetKeyDown(KeyCode.E) && gameObject.tag == "Time Option")
        {
            timeGate.SetActive(false);
            interactCanvas.SetActive(isVisible = false);
            Destroy(riddleConsole);
        }
        else if (collide.tag == "Player" && Input.GetKeyDown(KeyCode.E) && gameObject.tag == "Death Option")
        {
            deathGate.SetActive(false);
            interactCanvas.SetActive(isVisible = false);
            Destroy(riddleConsole);
        }
    }

    void OnTriggerExit(Collider collide)
    {
        interactCanvas.SetActive(isVisible = false);
    }
}
