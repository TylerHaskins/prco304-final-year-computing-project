﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TileEventHandler : UnityEvent<int> { }

public class TileEvent : MonoBehaviour {

    public int index;
    public Color resetColour = Color.grey;
    public Color activatedColour = Color.green;
    public TileEventHandler onTriggered;

    public GameObject tileOne;
    public GameObject tileTwo;
    public GameObject tileThree;
    public GameObject tileFour;
    public GameObject tileFive;
    public GameObject tileSix;
    public GameObject tileSeven;
    public GameObject tileEight;
    public GameObject tileNine;
    public GameObject resetTile;

    public GameObject hTilePuzzleGate;

    public bool tileOneActive = false;
    public bool tileTwoActive = false;
    public bool tileThreeActive = false;
    public bool tileFourActive = false;
    public bool tileFiveActive = false;
    public bool tileSixActive = false;
    public bool tileSevenActive = false;
    public bool tileEightActive = false;
    public bool tileNineActive = false;
    public bool allTilesActive = false;

    // Use this for initialization
    void Start () {
		
	}

    void Update()
    {
        if (allTilesActive == true)
        {
            //Destroy(hTilePuzzleGate);
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileTwo.GetComponent<Renderer>().material.color = activatedColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileSix.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = activatedColour;
            tileEight.GetComponent<Renderer>().material.color = activatedColour;
            tileNine.GetComponent<Renderer>().material.color = activatedColour;
        }
    }
	
	// Update is called once per frame
	void OnTriggerEnter (Collider other) {
        onTriggered.Invoke(index);
        if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 1")
        {
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileTwo.GetComponent<Renderer>().material.color = activatedColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileOneActive = true;
            tileTwoActive = true;
            tileThreeActive = false;
            tileFourActive = true;
            tileFiveActive = false;
            tileSixActive = false;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 2")
        {
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileThree.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileTwo.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = true;
            tileTwoActive = false;
            tileThreeActive = true;
            tileFourActive = false;
            tileFiveActive = true;
            tileSixActive = false;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 3")
        {
            tileThree.GetComponent<Renderer>().material.color = resetColour;
            tileTwo.GetComponent<Renderer>().material.color = activatedColour;
            tileSix.GetComponent<Renderer>().material.color = activatedColour;
            tileOneActive = false;
            tileTwoActive = true;
            tileThreeActive = false;
            tileFourActive = false;
            tileFiveActive = false;
            tileSixActive = true;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 4")
        {
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = activatedColour;
            tileFour.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = true;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = false;
            tileFiveActive = true;
            tileSixActive = false;
            tileSevenActive = true;
            tileEightActive = false;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 4" && tileOneActive == true && tileTwoActive == true && tileFourActive == true)
        {
            //print("Correct Tile In Sqeuence");
            tileOne.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = activatedColour;
             tileFour.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = true;
            tileFiveActive = true;
            tileSevenActive = true;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 5")
        {
            tileFive.GetComponent<Renderer>().material.color = resetColour;
            tileSix.GetComponent<Renderer>().material.color = activatedColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = true;
            tileFiveActive = false;
            tileSixActive = true;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 6")
        {
            tileNine.GetComponent<Renderer>().material.color = activatedColour;
            tileThree.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileSix.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = true;
            tileFourActive = false;
            tileFiveActive = true;
            tileSixActive = false;
            tileSevenActive = false;
            tileEightActive = false;
            tileNineActive = true;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 7")
        {
            tileEight.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = resetColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = true;
            tileFiveActive = false;
            tileSixActive = false;
            tileSevenActive = false;
            tileEightActive = true;
            tileNineActive = false;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 7" && tileOneActive == true && tileTwoActive == true && tileFourActive == true && tileFiveActive == true && tileSevenActive == true)
        {
            print("Correct Tile In Sqeuence");
            tileEight.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = resetColour;
            tileFour.GetComponent<Renderer>().material.color = activatedColour;
            tileFourActive = true;
            tileEightActive = true;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 8")
        {
            tileNine.GetComponent<Renderer>().material.color = activatedColour;
            tileSeven.GetComponent<Renderer>().material.color = activatedColour;
            tileFive.GetComponent<Renderer>().material.color = activatedColour;
            tileEight.GetComponent<Renderer>().material.color = resetColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = false;
            tileFiveActive = true;
            tileSixActive = false;
            tileSevenActive = true;
            tileEightActive = false;
            tileNineActive = true;
        }
        else if (other.gameObject.tag == "Player" && gameObject.tag == "Tile 9")
        {
            tileEight.GetComponent<Renderer>().material.color = activatedColour;
            tileNine.GetComponent<Renderer>().material.color = activatedColour;
            tileSix.GetComponent<Renderer>().material.color = activatedColour;
            tileOneActive = false;
            tileTwoActive = false;
            tileThreeActive = false;
            tileFourActive = false;
            tileFiveActive = false;
            tileSixActive = true;
            tileSevenActive = false;
            tileEightActive = true;
            tileNineActive = true;
        }
    }

    public void OnReset()
    {
        GetComponent<Renderer>().material.color = resetColour;
    }
}
