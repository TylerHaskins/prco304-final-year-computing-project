﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoCollectable3 : MonoBehaviour
{

    public GameObject interactCanvas;
    private bool isVisible = false;
    public bool ammoPickedUp = false;

    void Start()
    {
        interactCanvas.SetActive(isVisible);
    }

    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            interactCanvas.SetActive(isVisible = true);
            ammoPickedUp = true;
        }
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player 2D")
        {
            interactCanvas.SetActive(isVisible = true);
            ammoPickedUp = true;
        }
    }

    void OnTriggerStay(Collider collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player")
        {
            Destroy(gameObject);
            ammoPickedUp = false;
            interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (isVisible == true && Input.GetKeyDown(KeyCode.E) && collision.gameObject.tag == "Player 2D")
        {
            Destroy(gameObject);
            ammoPickedUp = false;
            interactCanvas.SetActive(isVisible = false);
        }
    }

    void OnTriggerExit(Collider collision)
    {
        interactCanvas.SetActive(isVisible = false);
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        interactCanvas.SetActive(isVisible = false);
    }
}
