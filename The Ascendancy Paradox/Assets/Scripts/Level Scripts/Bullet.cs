﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public GameObject interactCanvas;
    private Rigidbody bulletPrefab;
    private Transform myTransform;
    public int bulletDamage = 2;
    public string bulletDamageTagEnemy = "";
    public string bulletDamageTagEnemy1 = "";
    public string bulletDamageTagEnemy2 = "";
    public string bulletDamageTagEnemy3 = "";
    public string bulletDamageTagEnemyBoss = "";
    public string bulletDamageTagPlayer = "";
    public string bulletDamageTagExplosive = "";
    public float force = 0.4f;

    // Use this for initialization
    void Start()
    {
        bulletPrefab = GetComponent<Rigidbody>();
        myTransform = GetComponent<Transform>();

        bulletPrefab.AddForce(transform.forward * force, ForceMode.Impulse);

        Destroy(gameObject, 2.5f);
    }
	
	void OnTriggerEnter(Collider bulletCollide)
    {
        if (bulletCollide.CompareTag(bulletDamageTagEnemy))
        {
            Debug.Log("Bullet Impacted Enemy");
            bulletCollide.SendMessage("InflictDamageOnEnemy", bulletDamage);
        }
        else if (bulletCollide.CompareTag(bulletDamageTagEnemy1))
        {
            Debug.Log("Bullet Impacted Enemy");
            bulletCollide.SendMessage("InflictDamageOnEnemy1", bulletDamage);
        }
        else if (bulletCollide.CompareTag(bulletDamageTagEnemy2))
        {
            Debug.Log("Bullet Impacted Enemy");
            bulletCollide.SendMessage("InflictDamageOnEnemy2", bulletDamage);
        }
        else if (bulletCollide.CompareTag(bulletDamageTagEnemy3))
        {
            Debug.Log("Bullet Impacted Enemy");
            bulletCollide.SendMessage("InflictDamageOnEnemy3", bulletDamage);
        }
        else if (bulletCollide.CompareTag(bulletDamageTagEnemyBoss))
        {
            Debug.Log("Bullet Impacted Enemy");
            bulletCollide.SendMessage("InflictDamageOnEnemyBoss", bulletDamage);
        }

        else if (bulletCollide.CompareTag(bulletDamageTagExplosive))
        {
            Debug.Log("Bullet Impacted Explosive");
            bulletCollide.SendMessage("InflictDamageOnExplosive", bulletDamage);
        }

        else if (bulletCollide.CompareTag(bulletDamageTagPlayer))
        {
            interactCanvas.SetActive(false);
            Debug.Log("Bullet Impacted Player");
            bulletCollide.SendMessage("TakeDamage", bulletDamage);
        }
    }
}
