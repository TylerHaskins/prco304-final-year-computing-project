﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PistolFire : MonoBehaviour {

    public int damage = 2;
    public float fireRate = .25f;
    public float weaponRange = 50f;
    public float hitForce = 100f;
    public Transform pistolBarrel;

    private Camera fpsCamera;
    private WaitForSeconds shotDuration = new WaitForSeconds(.07f);
    public AudioSource bulletFireSoundEffect;
    private LineRenderer laserLine;
    private float nextFire;

    public AudioSource emptyClipFireSound;
    public AudioSource pistolReloadSound;
    public int bulletCount;
    private int clipSize;
    private int maxSingleClipSize;
    private int maxOverallClipSize;
    public bool shootingEnabled = true;
    private bool ammoIncreaseOnce = false;
    public Text ammunitionClipCount;

	void Start () {
        laserLine = GetComponent<LineRenderer>();
        fpsCamera = GetComponentInParent<Camera>();

        ammunitionClipCount.text = "0/0";
        shootingEnabled = false;
        clipSize = 0;
        bulletCount = 20;
        maxSingleClipSize = 20;
        maxOverallClipSize = 60;
        Debug.Log("Shooting Disabled");
    }

    void Update()
    {

        if (Input.GetMouseButtonDown(0) && Time.time > nextFire && shootingEnabled == true && bulletCount > 0)
        {
            nextFire = Time.time + fireRate;

            StartCoroutine(ShotEffect());

            Vector3 rayOrigin = fpsCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;

            laserLine.SetPosition(0, pistolBarrel.position);
            bulletCount--;
            UpdateAmmunitionText();

            if (Physics.Raycast(rayOrigin, fpsCamera.transform.forward, out hit, weaponRange))
            {
                laserLine.SetPosition(1, hit.point);

                EnemyBehaviour enemyHealth = hit.collider.GetComponent<EnemyBehaviour>();
                ExplosiveBehaviour explosiveHealth = hit.collider.GetComponent<ExplosiveBehaviour>();

                if (enemyHealth != null)
                {
                    enemyHealth.InflictDamageOnEnemy(damage);
                }
                
                if (explosiveHealth != null)
                {
                    explosiveHealth.InflictDamageOnExplosive(damage);
                }
            }
            else
            {
                laserLine.SetPosition(1, rayOrigin + (fpsCamera.transform.forward * weaponRange));
            }
        }
        else if (bulletCount <= 0 && Input.GetMouseButtonDown(0))
        {
            emptyClipFireSound.Play();
        }


        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 1") != null && GameObject.FindGameObjectWithTag("Ammo Clip 1").GetComponent<AmmoCollectable>().ammoPickedUp == true)
        {
            print("Ammo clip found");
            if (clipSize<maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
}
        }

        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 2") != null && GameObject.FindGameObjectWithTag("Ammo Clip 2").GetComponent<AmmoCollectable2>().ammoPickedUp == true)
        {
            if (clipSize<maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }

        if (ammoIncreaseOnce == false && GameObject.FindGameObjectWithTag("Ammo Clip 3") != null && GameObject.FindGameObjectWithTag("Ammo Clip 3").GetComponent<AmmoCollectable3>().ammoPickedUp == true)
        {
            if (clipSize<maxSingleClipSize)
            {
                clipSize += maxSingleClipSize;
                UpdateAmmunitionText();
                MakeAmmoIncreaseFalse();
            }
        }
        ReloadPistol();
	}

    private IEnumerator ShotEffect()
    {
        bulletFireSoundEffect.Play();

        laserLine.enabled = true;
        yield return shotDuration;
        laserLine.enabled = false;
    }

    void MakeAmmoIncreaseFalse()
    {
        ammoIncreaseOnce = false;
    }

    void ReloadPistol()
    {
        if (bulletCount <= 0 && clipSize >= 20 && Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Reload method called");
            shootingEnabled = false;
            Debug.Log("Shooting Disabled");
            bulletCount += 20;
            pistolReloadSound.Play();
            clipSize -= 20;
            UpdateAmmunitionText();
        }
        else if (clipSize < 1 && Input.GetKeyDown(KeyCode.R) || bulletCount <= 1 && clipSize <= 1 && Input.GetKeyDown(KeyCode.R))
        {
            emptyClipFireSound.Play();
        }
    }

    void UpdateAmmunitionText()
    {
        shootingEnabled = true;
        Debug.Log("Shooting Enabled");
        ammunitionClipCount.text = bulletCount + "/" + clipSize;
    }
}
