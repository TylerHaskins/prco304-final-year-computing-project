﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class optionsButton : MonoBehaviour {

    public AudioSource menuButtonClick;
    public string levelName;

    public void OnMenuOptionsClick()
    {
        menuButtonClick.Play(); //Play the audio clip linked with a button click
        SceneManager.LoadScene(levelName);
    }
}
