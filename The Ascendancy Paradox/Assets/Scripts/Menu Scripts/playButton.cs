﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class playButton : MonoBehaviour {

    public string levelName = "";
    public GameObject pauseMenu;
    public AudioSource menuButtonClick;

	public void OnMenuPlayClick() //Void for if game is in a main menu state.
    {
        SceneManager.LoadScene(levelName); //Use this function for the main menu play button to load into the tutorial level
        menuButtonClick.Play(); //Play the audio clip linked with a button click
    }

    public void OnGameplayResumeClick() //If the game is in Gameplay state.
    {
        Debug.Log("Click Sound Played"); //Play the audio clip linked with a button click
        pauseMenu.SetActive(false);
        Time.timeScale = 1.0f; //Pause menu play click during gameplay
        menuButtonClick.Play();
    }
}
