﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiddleRead : MonoBehaviour {

    public AudioSource riddleRead;
    private bool soundActivated = false;

	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && soundActivated == false)
        {
            riddleRead.Play();
            soundActivated = true;
        }
    }
}
